package com.mynt.exam.demo.utility;

import com.mynt.exam.demo.domain.ParcelDetails;
import com.mynt.exam.demo.enums.ReturnMessage;
import com.mynt.exam.demo.exception.CustomException;

public class Validation {
	
	public static void validateRequiredFields(ParcelDetails parcelDetails) throws CustomException{
		
		if(parcelDetails.getHeight() == null) {
			throw new CustomException(ReturnMessage.HEIGHT_REQUIRED.getValue());
		}
		if(parcelDetails.getLength() == null) {
			throw new CustomException(ReturnMessage.LENGHT_REQUIRED.getValue());
		}
		if(parcelDetails.getWeight() == null) {
			throw new CustomException(ReturnMessage.WEIGHT_REQUIRED .getValue());
		}
		if(parcelDetails.getWidth() == null) {
			throw new CustomException(ReturnMessage.WIDHT_REQUIRED.getValue());
		}
				
	}

}
