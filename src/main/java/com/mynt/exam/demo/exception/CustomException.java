package com.mynt.exam.demo.exception;

import com.mynt.exam.demo.enums.ReturnMessage;

public class CustomException extends Exception{
	/**
	 * The serial version UID
	 */
	private static final long serialVersionUID = 1856889107994819757L;


	/**
	 * Constructs an Exception with the specified message
	 * 
	 * @param message The Error Message
	 */
	public CustomException(final String messageKey) {
		super(messageKey);
	}

	/**
	 * Constructs an Exception with the specified message and Throwable Cause
	 * 
	 * @param message The Error Message
	 * @param t       The throwable
	 */
	public CustomException(final String message, final Throwable t) {
		super(message, t);
	}

	public CustomException(ReturnMessage message) {
		super(message.getValue());
	}
}
