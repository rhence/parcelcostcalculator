package com.mynt.exam.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mynt.exam.demo.enums.ReturnMessage;

public class Response {

	private String status;

	private String message;

	private Object data;

	private boolean reload;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isReload() {
		return reload;
	}

	public void setReload(boolean reload) {
		this.reload = reload;
	}
	
	public static Response respond(Object data, String status, String message) {
		Response instance = new Response();
		instance.setStatus(status);
		instance.setData(data);
		instance.setMessage(message);
		return instance;
	}

	@JsonIgnore
	public static Response success(Object data, String message) {
		return respond(data, ReturnMessage.SUCCESS.getValue(), message);
	}

	@JsonIgnore
	public static Response success(Object data, ReturnMessage message) {
		return success(data, message.getValue());
	}

	@JsonIgnore
	public static Response success() {
		return success((String) null);
	}

	@JsonIgnore
	public static Response success(Object data) {
		return success(data, (String) null);
	}

	@JsonIgnore
	public static Response success(String message) {
		return success(null, message);
	}

	@JsonIgnore
	public static Response success(ReturnMessage message) {
		return success(null, message.getValue());
	}

	@JsonIgnore
	public static Response error(Object data, String message) {
		return respond(data, ReturnMessage.ERROR.getValue(), message);
	}

	@JsonIgnore
	public static Response error(Object data, ReturnMessage message) {
		return error(data, message.getValue());
	}

	@JsonIgnore
	public static Response error(Object data) {
		return error(data, (String) null);
	}

	@JsonIgnore
	public static Response error(String message) {
		return error(null, message);
	}

	@JsonIgnore
	public static Response error(ReturnMessage message) {
		return error(null, message.getValue());
	}

	@JsonIgnore
	public static Response error() {
		return error(null, ReturnMessage.SOMETHING_WENT_WRONG.getValue());
	}

	@JsonIgnore
	public static Response warning(Object data, String message) {
		return respond(data, ReturnMessage.WARNING.getValue(), message);
	}

	@JsonIgnore
	public static Response warning(Object data, ReturnMessage message) {
		return warning(data, message.getValue());
	}

	@JsonIgnore
	public static Response warning(Object data) {
		return warning(data, (String) null);
	}

	@JsonIgnore
	public static Response warning(ReturnMessage message) {
		return warning(null, message.getValue());
	}

	@JsonIgnore
	public Response respondSuccess(Object data, String message) {
		return respond(data, ReturnMessage.SUCCESS.getValue(), message);
	}

	@JsonIgnore
	public Response respondSuccess(Object data) {
		return respondSuccess(data, null);
	}

	@JsonIgnore
	public Response respondError(Object data, String message) {
		return respond(data, ReturnMessage.ERROR.getValue(), message);
	}

	@JsonIgnore
	public Response respondError(Object data) {
		return respondError(data, null);
	}

	@JsonIgnore
	public Response respondError(String message) {
		return respondError(null, message);
	}

	@JsonIgnore
	public Response respondError() {
		return respondError(null, ReturnMessage.SOMETHING_WENT_WRONG.getValue());
	}

	@JsonIgnore
	public Response respondWarning(Object data, String message) {
		return respond(data, ReturnMessage.WARNING.getValue(), message);
	}

	@JsonIgnore
	public Response respondWarning(Object data) {
		return respondWarning(data, null);
	}

}

