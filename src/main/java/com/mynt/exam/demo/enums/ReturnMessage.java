package com.mynt.exam.demo.enums;

import java.text.MessageFormat;

public enum ReturnMessage {
	ERROR("Error"), WARNING("Warning"), SUCCESS("Success"), SOMETHING_WENT_WRONG("Something went wrong."),
	HEIGHT_REQUIRED("Height is required"),LENGHT_REQUIRED("Lenght is required"),
	WIDHT_REQUIRED("Width is required"),WEIGHT_REQUIRED("Weight is required");
	
	private String value;

	private ReturnMessage(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String format(Object... params) {
		return MessageFormat.format(this.value, params);
	}

}
