package com.mynt.exam.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mynt.exam.demo.domain.ParcelDetails;
import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.enums.ReturnMessage;
import com.mynt.exam.demo.exception.CustomException;
import com.mynt.exam.demo.service.CalculateService;
import com.mynt.exam.demo.utility.Validation;

@RestController
public class CalculateController {
	
	@Autowired
	CalculateService calculateService;
	
	@GetMapping("/calculate")
	public Response calculateCostOfDelivery(@RequestBody ParcelDetails parcelDetails){
		Response  result= new Response();
		
		// Validate fields
		try {
			Validation.validateRequiredFields(parcelDetails);
			result = calculateService.calculateDeliveryCost(parcelDetails);
		} catch (CustomException e) {
			result.setStatus(ReturnMessage.ERROR.getValue());
			result.setMessage(e.getMessage());
		}
		
		
		return result;
		
	}

}
