package com.mynt.exam.demo.service;

import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.exception.CustomException;

public interface VoucherService {
	
	public Response getVoucerDiscount(String voucherCode) throws CustomException;

}
