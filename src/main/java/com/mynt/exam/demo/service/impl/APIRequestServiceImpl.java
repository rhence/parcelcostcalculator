package com.mynt.exam.demo.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.exception.CustomException;
import com.mynt.exam.demo.service.APIRequestService;

@Service
public class APIRequestServiceImpl implements APIRequestService {

	@Override
	public Response getAPIRequest(String baseURL, String subPage, String parameter, String key) throws CustomException {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		StringBuilder url = new StringBuilder();
		url.append(baseURL).append("/").append(subPage).append("/").append(parameter).append("?key=").append(key);
		// String url = baseURL+"/"+subPage+"/"+parameter+"?key=" + key;
		Response result = new Response();
		try {
			result.setData(restTemplate.getForObject(url.toString(), String.class));
		} catch (RestClientException e) {
			throw new CustomException(e.getMessage());
		}
		return result;
	}

}
