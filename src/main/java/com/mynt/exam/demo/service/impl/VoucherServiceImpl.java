package com.mynt.exam.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.exception.CustomException;
import com.mynt.exam.demo.service.APIRequestService;
import com.mynt.exam.demo.service.VoucherService;

@Service
public class VoucherServiceImpl implements VoucherService{
	
	@Autowired
	APIRequestService apiRequestService;

	@Override
	public Response getVoucerDiscount(String voucherCode) throws CustomException{
		// TODO Auto-generated method stub
		String baseUrl = "https://mynt-exam.mocklab.io";
		String subPage = "voucher";
		String key = "apikey";
		Response result = new Response();
		try {
			result = apiRequestService.getAPIRequest(baseUrl, subPage, voucherCode, key);
		} catch (CustomException e) {
			throw new CustomException(e.getMessage());
		}
		//if(response.get)
		return result;
	}

}
