package com.mynt.exam.demo.service;

import com.mynt.exam.demo.domain.ParcelDetails;
import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.exception.CustomException;

public interface CalculateService {
	
	public Response calculateDeliveryCost(ParcelDetails parcelDetails) throws CustomException;

}
