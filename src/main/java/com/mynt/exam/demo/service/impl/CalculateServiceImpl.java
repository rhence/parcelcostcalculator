package com.mynt.exam.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mynt.exam.demo.domain.ParcelDetails;
import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.domain.VoucherDetails;
import com.mynt.exam.demo.enums.ReturnMessage;
import com.mynt.exam.demo.exception.CustomException;
import com.mynt.exam.demo.service.CalculateService;
import com.mynt.exam.demo.service.VoucherService;

import net.minidev.json.JSONObject;

@Service
public class CalculateServiceImpl implements CalculateService {

	@Value("${reject.condition}")
	private double rejectCondition;
	@Value("${heavy.parcel.condition}")
	private double heavyParcelCondtion;
	@Value("${heavy.parcel.price}")
	private double heavyParcelPrice;
	@Value("${small.parcel.condition}")
	private double smallParcelCondition;
	@Value("${small.parcel.price}")
	private double smallParcelPrice;
	@Value("${medium.parcel.condition}")
	private double mediumParcelCondition;
	@Value("${medium.parcel.price}")
	private double mediumParcelPrice;
	@Value("${large.parcel.price}")
	private double largeParcelPrice;

	@Autowired
	VoucherService voucherService;

	@Override
	public Response calculateDeliveryCost(ParcelDetails parcelDetails) throws CustomException {

		Response result = new Response();
		double volume = parcelDetails.getHeight() * parcelDetails.getWidth() * parcelDetails.getLength();
		JSONObject totalCost = new JSONObject();

		// Compute cost of parcel
		if (parcelDetails.getWeight() > rejectCondition) {
			totalCost.put("Rule Name", "Reject");
			totalCost.put("Amount", "N/A");
		} else if (parcelDetails.getWeight() > heavyParcelCondtion && parcelDetails.getWeight() <= rejectCondition) {
			totalCost.put("Rule Name", "Heavy Parcel");
			totalCost.put("Amount", heavyParcelPrice * parcelDetails.getWeight());
		} else if (volume < smallParcelCondition) {
			totalCost.put("Rule Name", "Small Parcel");
			totalCost.put("Amount", smallParcelPrice * volume);
		} else if (volume < mediumParcelCondition && volume > smallParcelCondition) {
			totalCost.put("Rule Name", "Medium Parcel");
			totalCost.put("Amount", mediumParcelPrice * volume);
		} else {
			totalCost.put("Rule Name", "Large Parcel");
			totalCost.put("Amount", largeParcelPrice * volume);
		}

		// Check if has voucher
		if (parcelDetails.getVoucher() != "" || parcelDetails.getVoucher() != null) {
			Response voucherResult;
			try {
				voucherResult = voucherService.getVoucerDiscount(parcelDetails.getVoucher());
				Gson stringToJson = new Gson();
				VoucherDetails voucher = stringToJson.fromJson((String) voucherResult.getData(), VoucherDetails.class);
				double discount = voucher.getDiscount();
				double discountedAmount = (double) totalCost.get("Amount") - discount;
				totalCost.put("Discount", discount);
				totalCost.put("Discounted Amount", discountedAmount);
			} catch (CustomException e) {
				throw new CustomException(e.getMessage());
			}
		}
		result.setMessage(ReturnMessage.SUCCESS.getValue());
		result.setData(totalCost);
		return result;
	}

}
