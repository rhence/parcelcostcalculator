package com.mynt.exam.demo.service;

import com.mynt.exam.demo.domain.Response;
import com.mynt.exam.demo.exception.CustomException;

public interface APIRequestService {
	
	public Response getAPIRequest(String baseURL, String subPage, String parameter, String key) throws CustomException;

}
